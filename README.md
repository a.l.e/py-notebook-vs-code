# Jupyter notebooks in Visual Studio Code

## Installation

- Install VS Code
- Install Python
- In VS Code add the Python extension by Microsoft

## Jupyter notebooks

- The Python extension also installs the Jupyter one
- Create a new notebook: `ctrl-shift-p create notebook`

## Using virtual environments

In the terminal (in any terminal...) create a venv and activate it.

Then:

```
python -m ipykernel install --user --name venv --display-name "Python (venv)"
```

This will add some settings in `/home/your-username/.local/share/jupyter/kernels/venv`.

Restart VS Code and you can now pick your venv in the right upper corner

- [jupyter notebook running kernel in different env](https://stackoverflow.com/a/37891993/5239250)

## Sample notebooks

### Buttons and widgets

- [copy-to-clipboard.ipynb](copy-to-clipboard.ipynb)
- `pip install ipywidgets`
- [Widget Events](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Events.html)

### Reading XML
- [xml-reading.ipynb](xml-reading.ipynb)